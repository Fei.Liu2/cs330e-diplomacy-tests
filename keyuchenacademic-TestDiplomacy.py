#!/usr/bin/env python3

# ------------------------------
# projects/diplomacy/RunDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# ------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):

    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Barcelona Hold\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i, 'A')
        self.assertEqual(j, "Barcelona")
        self.assertEqual(k, ["Hold"])

    def test_read_2(self):
        s = "B Austin Support A\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i, "B")
        self.assertEqual(j, "Austin")
        self.assertEqual(k, ["Support", "A"])

    def test_read_3(self):
        s = "C Madrid Move Austin\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i, "C")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, ["Move", "Austin"])


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, "A", "Austin")
        self.assertEqual(w.getvalue(), "A Austin\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "B", "[dead]")
        self.assertEqual(w.getvalue(), "B [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, "C", "Boston")
        self.assertEqual(w.getvalue(), "C Boston\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()