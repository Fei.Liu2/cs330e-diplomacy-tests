#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = StringIO("A Madrid Hold\n")
        inData = diplomacy_read(s)
        self.assertEqual(inData[0],  "A Madrid Hold")

    def test_read1(self):
        s = StringIO("B Barcelona Move Madrid\n")
        inData = diplomacy_read(s)
        self.assertEqual(inData[0],  "B Barcelona Move Madrid")

    def test_read2(self):
        s = StringIO("C London Support B\n")
        inData = diplomacy_read(s)
        self.assertEqual(inData[0],  "C London Support B")

    def test_read3(self):
        s = StringIO("D Austin Move London\n")
        inData = diplomacy_read(s)
        self.assertEqual(inData[0],  "D Austin Move London")

    # -----
    # print
    # -----

    def test_print(self):
        writer = StringIO()
        diplomacy_print(writer, None, "A Madrid Hold\n")
        self.assertEqual(writer.getvalue(), "A Madrid Hold\n")

    def test_print1(self):
        writer = StringIO()
        diplomacy_print(writer, None, "B Barcelona Move Madrid\n")
        self.assertEqual(writer.getvalue(), "B Barcelona Move Madrid\n")

    def test_print2(self):
        writer = StringIO()
        diplomacy_print(writer, None, "C London Support B\n")
        self.assertEqual(writer.getvalue(), "C London Support B\n")

    def test_print3(self):
        writer = StringIO()
        diplomacy_print(writer, None, "D Austin Move London\n")
        self.assertEqual(writer.getvalue(), "D Austin Move London\n")
 
    # -----
    # solve
    # -----

    def test_solve(self):
        rawData = StringIO("A Madrid Hold\n")
        writer = StringIO()
        diplomacy_solve(rawData, writer)
        self.assertEqual(
            writer.getvalue(), "A Madrid\n")

    def test_solve1(self):
        rawData = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        )
        writer = StringIO()
        diplomacy_solve(rawData, writer)
        self.assertEqual(
            writer.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve2(self):
        rawData = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        writer = StringIO()
        diplomacy_solve(rawData, writer)
        self.assertEqual(
            writer.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve3(self):
        rawData = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n"
        )
        writer = StringIO()
        diplomacy_solve(rawData, writer)
        self.assertEqual(
            writer.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve4(self):
        rawData = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n"
        )
        writer = StringIO()
        diplomacy_solve(rawData, writer)
        self.assertEqual(
            writer.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1

$ cat TestDiplomacy.out

$ coverage report -m                   >> TestDiplomacy.out

$ cat TestDiplomacy.out

.............
----------------------------------------------------------------------
Ran 13 tests in 0.005s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          71      0     52      0   100%
TestDiplomacy.py      63      0      0      0   100%
--------------------------------------------------------------
TOTAL                134      0     52      0   100%


"""