from io import StringIO
from unittest import main, TestCase

from Diplomacy import Army, diplomacy_read, diplomacy_support, diplomacy_kill, diplomacy_solve, diplomacy_move, diplomacy_solve

class TestDiplomacy (TestCase):
	# ----
	# read
	# ----

	def test_read1(self):
		s = "A Madrid Hold\n"
		army = diplomacy_read(s)
		resultString = ""
		resultString += army.name + " "  + army.status + "\n"
		self.assertEqual(resultString, "A Madrid\n")

	def test_read2(self):
		s = "B Barcelona Move Madrid\n"
		army = diplomacy_read(s)
		resultString = ""
		resultString += army.name + " "  + army.status + "\n"
		self.assertEqual(resultString, "B Barcelona\n")
	
	def test_read3(self):
		s = "C London Support B\n"
		army = diplomacy_read(s)
		resultString = ""
		resultString += army.name + " "  + army.status + "\n"
		self.assertEqual(resultString, "C London\n")

	# -------
	# support
	# -------

	def test_support1(self):
		listArmies = []
		listArmies.append(Army("A", "Madrid", "Hold"))
		listArmies.append(Army("B", "Barcelona", "Move", "Madrid"))
		listArmies.append(Army("C", "London", "Support", "B"))
		listArmies.append(Army("D", "Austin", "Move", "London"))

		resultString = ""
		resultList = diplomacy_support(listArmies)
		for army in resultList:
			resultString += army.name + " " + army.status + " " + str(army.point) + "\n"
		self.assertEqual(resultString, "A Madrid 0\nB Barcelona 0\nC London 0\nD Austin 0\n")

	def test_support2(self):
		listArmies = []
		listArmies.append(Army("A", "Madrid", "Hold"))
		listArmies.append(Army("B", "Barcelona", "Move", "Madrid"))
		listArmies.append(Army("C", "London", "Move", "Madrid"))
		listArmies.append(Army("D", "Paris", "Support", "B"))

		resultString = ""
		resultList = diplomacy_support(listArmies)
		for army in resultList:
			resultString += army.name + " " + army.status + " " + str(army.point) + "\n"
		self.assertEqual(resultString, "A Madrid 0\nB Barcelona 1\nC London 0\nD Paris 0\n")

	def test_support3(self):
		listArmies = []
		listArmies.append(Army("A", "Madrid", "Hold"))
		listArmies.append(Army("B", "Barcelona", "Move", "Madrid"))
		listArmies.append(Army("C", "London", "Move", "Madrid"))
		listArmies.append(Army("D", "Paris", "Support", "B"))
		listArmies.append(Army("E", "Austin", "Support", "A"))

		resultString = ""
		resultList = diplomacy_support(listArmies)
		for army in resultList:
			resultString += army.name + " " + army.status + " " + str(army.point) + "\n"
		self.assertEqual(resultString, "A Madrid 1\nB Barcelona 1\nC London 0\nD Paris 0\nE Austin 0\n")

	# ----
	# move
	# ----

	def test_move1(self):
		listArmies = []
		listArmies.append(Army("A", "Madrid", "Hold"))
		listArmies.append(Army("B", "Barcelona", "Move", "Madrid"))
		listArmies.append(Army("C", "London", "Support", "B"))
		listArmies.append(Army("D", "Austin", "Move", "London"))

		resultString = ""
		resultDict = diplomacy_move(listArmies)
		for city in resultDict:
			resultString += city + ": "
			for army in resultDict[city]:
				resultString += army.name
			resultString += "\n"

		self.assertEqual(resultString, "Madrid: AB\nLondon: CD\n")

	def test_move2(self):
		listArmies = []
		listArmies.append(Army("A", "Madrid", "Hold"))
		listArmies.append(Army("B", "Barcelona", "Move", "Madrid"))
		listArmies.append(Army("C", "London", "Move", "Madrid"))
		listArmies.append(Army("D", "Paris", "Support", "B"))

		resultString = ""
		resultDict = diplomacy_move(listArmies)
		for city in resultDict:
			resultString += city + ": "
			for army in resultDict[city]:
				resultString += army.name
			resultString += "\n"

		self.assertEqual(resultString, "Madrid: ABC\nParis: D\n")

	def test_move3(self):
		listArmies = []
		listArmies.append(Army("A", "Madrid", "Hold"))
		listArmies.append(Army("B", "Barcelona", "Move", "Madrid"))
		listArmies.append(Army("C", "London", "Move", "Madrid"))
		listArmies.append(Army("D", "Paris", "Support", "B"))
		listArmies.append(Army("E", "Austin", "Support", "A"))

		resultString = ""
		resultDict = diplomacy_move(listArmies)
		for city in resultDict:
			resultString += city + ": "
			for army in resultDict[city]:
				resultString += army.name
			resultString += "\n"

		self.assertEqual(resultString, "Madrid: ABC\nParis: D\nAustin: E\n")

	# ----
	# kill
	# ----

	def test_kill1(self):
		listArmies = []
		listArmies.append(Army("A", "Madrid", "Hold"))
		listArmies.append(Army("B", "Barcelona", "Move", "Madrid"))
		listArmies.append(Army("C", "London", "Support", "B"))
		listArmies.append(Army("D", "Austin", "Move", "London"))

		supportList = diplomacy_support(listArmies)
		cityDict = diplomacy_move(supportList)
		killDict = diplomacy_kill(cityDict)

		resultString = ""
		for city in killDict:
			for army in killDict[city]:
				resultString += army.name + " " + army.status + "\n"

		self.assertEqual(resultString, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

	def test_kill2(self):
		listArmies = []
		listArmies.append(Army("A", "Madrid", "Hold"))
		listArmies.append(Army("B", "Barcelona", "Move", "Madrid"))
		listArmies.append(Army("C", "London", "Move", "Madrid"))
		listArmies.append(Army("D", "Paris", "Support", "B"))

		supportList = diplomacy_support(listArmies)
		cityDict = diplomacy_move(supportList)
		killDict = diplomacy_kill(cityDict)

		resultString = ""
		for city in killDict:
			for army in killDict[city]:
				resultString += army.name + " " + army.status + "\n"

		self.assertEqual(resultString, "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

	def test_kill3(self):
		listArmies = []
		listArmies.append(Army("A", "Madrid", "Hold"))
		listArmies.append(Army("B", "Barcelona", "Move", "Madrid"))
		listArmies.append(Army("C", "London", "Move", "Madrid"))
		listArmies.append(Army("D", "Paris", "Support", "B"))
		listArmies.append(Army("E", "Austin", "Support", "A"))

		supportList = diplomacy_support(listArmies)
		cityDict = diplomacy_move(supportList)
		killDict = diplomacy_kill(cityDict)

		resultString = ""
		for city in killDict:
			for army in killDict[city]:
				resultString += army.name + " " + army.status + "\n"

		self.assertEqual(resultString, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

	# -----
	# solve
	# -----

	def test_solve1(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

	def test_solve2(self):
		r = StringIO("A Madrid Hold")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), "A Madrid\n")

	def test_solve3(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
# ----
# main
# ----

if __name__ == "__main__":
	main()
